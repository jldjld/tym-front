import React, { useCallback } from 'react';
import { useFonts } from 'expo-font';
import * as SplashScreen from 'expo-splash-screen';
import AppNav from './src/navigation/AppNav';
import { AuthProvider } from './contexts/AuthContext';

SplashScreen.preventAutoHideAsync();

function App() {
  
    const [fontsLoaded] = useFonts({
        'Inter-Bold': require('./assets/fonts/Inter-Bold.ttf'),
        'Inter-SemiBold': require('./assets/fonts/Inter-SemiBold.ttf'),
    });

    const onLayoutRootView = useCallback(async () => {
        if (fontsLoaded) {
          await SplashScreen.hideAsync();
        }
      }, [fontsLoaded]);

      if (!fontsLoaded) {
        return null;
      }    

    return (
      <AuthProvider >
        <AppNav onLayout={onLayoutRootView()} />
      </AuthProvider>
      );
}

export default App;