import { View, Text, StyleSheet, Image } from 'react-native'
import React, { useRef, useState } from 'react'
import { TextInput } from 'react-native-paper';
import { TouchableOpacity } from 'react-native';

const showPasswordIcon = require('../../assets/icons/vue.png');

const RegisterForm = ({clubName, setClubName, email, password, setEmail, setPassword, userRef, gender, setGender}) => {
    const [hidePassword, setHidePassword] = useState(true);


    // Show or hide password while typing
    const togglePasswordVisibility = () => {
      setHidePassword(!hidePassword);
    };

    return (
        <View style={styles.loginFields}>
        <TextInput 
                value={email}
                style={styles.input}
                placeholder="adresse email..."
                // Disable uppercases for email input
                keyboardType="email-address"
                autoCapitalize="none"
                onChangeText={ (text: string) => setEmail(text)}
                ref={userRef} />
            <View style={styles.pwdInput}>
                <TextInput
                    value={password}
                    style={styles.input}
                    placeholder="mot de passe..."
                    onChangeText={ (text: string) => setPassword(text)}
                    secureTextEntry={hidePassword} />
                <TouchableOpacity onPress={togglePasswordVisibility} style={styles.showPassword}>
                    <Image source={showPasswordIcon} style={styles.icon} /> 
                </TouchableOpacity>
            </View>
        </View>
    )
}

const styles = StyleSheet.create({
    loginFields: {
        bottom: '0%',
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: 'rgba(248, 248, 248, 1)',
        borderTopLeftRadius: 20,
        borderTopRightRadius: 20,
        width: '100%',
        fontSize: 10,
        paddingVertical: 50
    },
    input: {
        fontSize: 15,
        fontFamily: 'Inter-SemiBold',
        borderColor: 'rgba(220, 212, 212, 1)',
        borderWidth: 1,
        borderRadius: 10,
        color: 'rgba(115, 115, 115, 1)',
        padding: 5,
        paddingHorizontal: 20,
        width: '65%',
        marginBottom:15,
    },
    pwdInput: {
        flexDirection: 'row',
        alignItems: 'center'
    },
    icon: { 
        width: 16,
        height: 16
    },
    showPassword: {
        right: 10,
        top: 12,
        position: 'absolute'
    },
});

export default Auth;