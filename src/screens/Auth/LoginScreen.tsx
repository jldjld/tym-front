import axios from 'axios';
import React, { useState, useRef, useEffect } from 'react';
import { Image, ImageBackground, StyleSheet, Text, TextInput, TouchableOpacity, View } from 'react-native';
import { useAuth } from '../../../contexts/AuthContext';

const showPasswordIcon = require('../../../assets/icons/vue.png');

const LoginScreen = ({navigation}) => {
    const {onLogin}                       = useAuth();
    const userRef                         = useRef(null);
    const [email, setEmail ]              = useState(null);
    const [password, setPassword ]        = useState(null);
	const [errMsg, setErrMsg]             = useState('');
    const [hidePassword, setHidePassword] = useState(true);
    

    // Show or hide password while typing
    const togglePasswordVisibility = () => {
      setHidePassword(!hidePassword);
    };

    useEffect(() => {
		userRef.current.focus();
	}, []);


    const login = async () => {
        try {
            const auth = await onLogin!(email, password);
            if (auth && auth.error) {
                setErrMsg(auth.msg);
                alert(auth.msg);
            }
        } catch (error) {
            setErrMsg(error);
        }
    };

    return(
        <View>
            <ImageBackground source={require('../../../assets/colifting_home_img.jpg')} style={styles.image}>
                <View style={styles.loginInputs} >
                    <TextInput 
                        value={email}
                        style={styles.input}
                        placeholder="adresse email..."
                        // Disable uppercases for email input
                        keyboardType="email-address"
                        autoCapitalize="none"
                        onChangeText={ (text: string) => setEmail(text)}
                        ref={userRef} />
                    <View style={styles.pwdInput}>
                        <TextInput
                            value={password}
                            style={styles.input}
                            placeholder="mot de passe..."
                            onChangeText={ (text: string) => setPassword(text)}
                            secureTextEntry={hidePassword} />
                        <TouchableOpacity onPress={togglePasswordVisibility} style={styles.showPassword}>
                            <Image source={showPasswordIcon} style={styles.icon} /> 
                        </TouchableOpacity>
                    </View>
                    <TouchableOpacity style={styles.loginBtn} onPress={login}>
                        <Text style={styles.loginBtnLabel}>Se connecter</Text>
                    </TouchableOpacity>
                    <View style={styles.register} >
                        <Text>Pas encore de compte ?</Text>
                        <TouchableOpacity
                            onPress={() => navigation.navigate('Register')} >
                            <Text style={styles.registerLink}> S'inscrire</Text>
                        </TouchableOpacity>
                    </View>
                </View>
            </ImageBackground>
        </View>
    )
}


export default LoginScreen;

const styles = StyleSheet.create({
    image: {
        backgroundRepeat: 'no-repeat',
        backgroundAttachment: 'fixed',
        height: '100%',
        backgroundsize: 'cover',
        backgroundPosition: '75% 50%',
        position: 'relative',
    },
    errorNotice: {
        color: 'red',
        fontFamily: 'Inter-Bold',
        fontSize: 8,
        marginBottom: 15
    },
    input: {
        fontSize: 15,
        fontFamily: 'Inter-SemiBold',
        borderColor: 'rgba(220, 212, 212, 1)',
        borderWidth: 1,
        borderRadius: 10,
        color: 'rgba(115, 115, 115, 1)',
        padding: 5,
        paddingHorizontal: 20,
        width: '65%',
        marginBottom:15,
    },
    pwdInput: {
        flexDirection: 'row',
        alignItems: 'center'
    },
    icon: { 
        width: 16,
        height: 16
    },
    showPassword: {
        right: 10,
        top: 12,
        position: 'absolute'
    },
    loginInputs: {
        position: 'absolute',
        flex: 1,
        bottom: '0%',
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: 'rgba(248, 248, 248, 1)',
        borderTopLeftRadius: 20,
        borderTopRightRadius: 20,
        width: '100%',
        fontSize: 10,
        paddingVertical: 50
    },
    loginBtn: {
        width: '65%',
        alignItems: 'center',
        flex: 1,
        backgroundColor: 'rgba(39, 39, 39, 1)',
        borderRadius: 20,
        padding: 20,
        elevation: 5,
        shadowColor: 'rgba(39, 39, 39, 1)',
    },
    loginBtnLabel : {
        color: 'rgba(248, 248, 248, 1)',
        fontFamily: 'Inter-Bold',
        fontSize: 15,
    },
    register: {
        flexDirection: 'row',
        paddingTop: 15
    },
    registerLink: {
        color: 'blue',
        fontFamily: 'Inter-Bold',
    }
});
