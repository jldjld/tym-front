import axios from 'axios';
import React, { useEffect, useRef, useState } from 'react';
import { Image, ImageBackground, StyleSheet, Text, TouchableOpacity, View } from 'react-native';
import { RadioButton, TextInput } from 'react-native-paper';
import { BASE_URL } from '../../../config';
import { Dropdown } from 'react-native-element-dropdown';
import apiClient from '../../services/customAxios';
import showPasswordIcon from '../../../assets/icons/vue.png';
import bckGrndImg from '../../../assets/colifting_home_img.jpg';
import { Availabilities } from '../../enums/Availabilities';

const RegisterScreen = ({navigation}) => {
    const [ city, setCity ]     = useState('');
    const [gyms, setGyms]       = useState([]);
    const [errMsg, setErrMsg]   = useState('');
    const [gymName, setgymName] = useState(null);
    const [isFocus, setIsFocus] = useState(false);
    const [gender, setGender]   = useState(null);
    const [availability, setAvailability] = useState(null);

    // User data
    const userRef                         = useRef(null);
    const [email, setEmail ]              = useState(null);
    const [password, setPassword ]        = useState({ first: '', second: '' });
    const [hidePassword, setHidePassword] = useState(true);
    const [pwdErrMsg, setPwdErrMsg]       = useState('');

    /**
     * Password format verification
     */
    // Show or hide password while typing
    const togglePasswordVisibility = () => {
        setHidePassword(!hidePassword);
    };

    const handleConfirmPassword = (pwdSecond :string) => {
        setPassword({ ...password, second: pwdSecond });
        if (password.first == pwdSecond) {
            setPwdErrMsg('');
        } else {
            setPwdErrMsg("Les mots de passe ne correspondent pas");
        }
    }

    /**
     * Verify password format
     * @param pwdFirst 
     */
    const verifyPasswordFormat = (pwdFirst: string) => {
        // Password must contain at least 8 characters, 1 uppercase, 1 lowercase, 1 number and 1 special
        const pwdRegex = /^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[a-zA-Z]).{8,}$/gm;
        const isValidLength = pwdFirst.length >= 8;
        const hasUppercase = /[A-Z]/.test(pwdFirst);
        const hasLowercase = /[a-z]/.test(pwdFirst);
        const hasNumber = /\d/.test(pwdFirst);
        const hasSpecialChar = /[!@#$%^&*(),.?":{}|<>]/.test(pwdFirst);

        setPwdErrMsg('');

        if (!isValidLength) {
            setPwdErrMsg('Le mot de passe doit contenir au moins 8 caractères');
        } else if (!hasUppercase) {
            setPwdErrMsg('Le mot de passe doit contenir au moins 1 majuscule');
        } else if (!hasLowercase) {
            setPwdErrMsg('Le mot de passe doit contenir au moins 1 minuscule');
        } else if (!hasNumber) {
            setPwdErrMsg('Le mot de passe doit contenir au moins 1 chiffre');
        } else if (!hasSpecialChar) {
            setPwdErrMsg('Le mot de passe doit contenir au moins 1 caractère spécial');
        }
    }

    /**
     * 
     * @param city 
     * @returns 
     */
    const findCityGyms = async (city: string) => {
        if(!city || city === '' || city == null ) {
            setErrMsg('Merci de sélectionner une ville');
            return;
        }

        // Verify that the city contains only letters
        if (!/^[a-zA-Z]+$/.test(city)) {
            setErrMsg('La ville ne doit contenir que des lettres');
            return;
        }

        city = city.toLowerCase();
        setCity(city);
        try {
            const getGyms = await apiClient.get( BASE_URL + '/gyms/location/' + city);
            if (getGyms && getGyms.data && getGyms.data.length > 0) {
                // Reset error message if gyms found
                setErrMsg('');
                setGyms(getGyms.data);
            }

        } catch (error) {
            setGyms([]); // Reset the gyms state to an empty array
            setErrMsg('Pas encore de salles de sport disponibles pour cette ville :)');
            //console.log(error);
            return {error: true, msg: (error as any).response.data.msg };
        }

        
    };

    return (
        <View>
        <ImageBackground source={bckGrndImg} style={styles.image}>
                <View style={styles.registerInputs}>
                    <Text style={styles.errorNotice}>{ errMsg ? errMsg : "" }</Text>
                    <Text style={styles.findGymsNotice}>La ville la plus proche de chez vous ? </Text>
                    <View style={styles.selectCity}>
                        <TouchableOpacity style={styles.radioTxt} activeOpacity={0.1} onPress={() => findCityGyms('nantes')}>
                            <Text >Nantes</Text>
                        </TouchableOpacity>
                        <TouchableOpacity style={styles.radioTxt} onPress={() => findCityGyms('lyon')}>
                            <Text>Lyon</Text>
                        </TouchableOpacity>
                    </View>
                    { gyms && gyms.length > 0 ? (
                        <View style={styles.dropdownContainer}>
                            <Dropdown
                                style={[styles.dropdown]}
                                placeholderStyle={styles.dropdownClub}
                                data={gyms}
                                value={gymName}
                                maxHeight={300}
                                placeholder="Club"
                                labelField={"name"}
                                valueField={"name"}
                                onFocus={() => setIsFocus(true)}
                                onBlur={() => setIsFocus(false)}
                                onChange={gym => {
                                    setgymName(gym.name);
                                    setIsFocus(false);
                                }}
                            />
                            <View style={styles.selectGender}>
                                <View style={styles.selectGenderRadio} >
                                    <RadioButton.Group onValueChange={setGender} value={gender}>
                                        <View style={styles.selectGenderRadio} >
                                            <RadioButton value="f"/>
                                            <Text style={styles.selectGenderRadioTxt}>F</Text>
                                        </View>
                                        <View style={styles.selectGenderRadio}>
                                            <RadioButton value="m"/>
                                            <Text style={styles.selectGenderRadioTxt} >M</Text>
                                        </View>
                                    </RadioButton.Group>
                                </View>
                            </View>
                            <View>
                                <Text style={styles.dropdownClub}>Objectifs</Text>
                            </View>
                            <Dropdown
                                style={[styles.dropdown]}
                                placeholderStyle={styles.dropdownClub}
                                data={Object.values(Availabilities) as any []}
                                value={availability}
                                maxHeight={300}
                                placeholder="Disponibilités"
                                labelField={Availabilities.AFTERNOON}
                                valueField={Availabilities.AFTERNOON}
                                onFocus={() => setIsFocus(true)}
                                onBlur={() => setIsFocus(false)}
                                onChange={availability => {
                                    setAvailability(availability);
                                    setIsFocus(false);
                                }}
                            />
                            <View>
                                <TextInput 
                                value={email}
                                style={styles.input}
                                underlineColorAndroid="transparent"
                                placeholder="Adresse email"
                                // Disable uppercases for email input
                                keyboardType="email-address"
                                autoCapitalize="none"
                                onChangeText={ (text: string) => setEmail(text)}
                                ref={userRef} />
                                <TextInput
                                    value={password.first}
                                    style={styles.input}
                                    placeholder="Mot de passe"
                                    onChangeText={(pwdFirst: string) => {
                                        setPassword({ ...password, first: pwdFirst });
                                        verifyPasswordFormat(pwdFirst);
                                    }}
                                    secureTextEntry={hidePassword}
                                />
                                <TouchableOpacity onPress={togglePasswordVisibility} style={styles.showPassword}>
                                    <Image source={showPasswordIcon} style={styles.icon} />
                                </TouchableOpacity>
                                <TextInput
                                    value={password.second}
                                    style={styles.input}
                                    placeholder="Confirmer le mot de passe"
                                    onChangeText={(pwdSecond: string) => {
                                        handleConfirmPassword(pwdSecond);
                                    }}
                                    secureTextEntry={hidePassword}
                                />
                                <TouchableOpacity onPress={togglePasswordVisibility} style={styles.showConfirmPassword}>
                                    <Image source={showPasswordIcon} style={styles.confirmIcon} />
                                </TouchableOpacity>
                                <Text style={styles.errorNotice}>{ pwdErrMsg ? pwdErrMsg : "" }</Text>
                            </View>
                        </View>
                        ) : ""
                    }

                <TouchableOpacity style={styles.findGymsBtn} >
                    <Text style={styles.findGymsLabel} >S'inscrire</Text>
                </TouchableOpacity>
                <View style={styles.login} >
                    <Text>Déja inscrit ?</Text>
                    <TouchableOpacity
                        onPress={() => navigation.navigate('Login')} >
                        <Text style={styles.loginLink}> Me connecter</Text>
                    </TouchableOpacity>
                </View>
            </View>
        </ImageBackground>
        </View>
    )
}

export default RegisterScreen;

const styles = StyleSheet.create({
    image: {
        backgroundRepeat: 'no-repeat',
        backgroundAttachment: 'fixed',
        height: '100%',
        backgroundsize: 'cover',
        backgroundPosition: '75% 50%',
        position: 'relative',
    },
    errorNotice: {
        color: 'red',
        marginBottom: 10,
        fontFamily: 'Inter-SemiBold',
        fontSize: 10,
    },
    radioTxt: {
        borderColor: 'rgba(220, 212, 212, 1)',
        borderWidth: 1,
        borderRadius: 10,
        color: 'white',
        paddingVertical: 10,
        paddingHorizontal: 5,
        width: '30%',
        marginHorizontal: 8,
        justifyContent: 'center',
        alignItems: 'center',
        fontFamily: 'Inter-SemiBold',
    },
    dropdownContainer: {
        width: '60%',
        marginBottom: 10
    },
    dropdown: {
        textAlign: 'flex-start',
        width: '100%',
        borderBottomWidth: 1,
    },
    dropdownClub: {
        fontFamily: 'Inter-SemiBold',
        fontWeight: 'bold',
        textTransform: 'uppercase',
        fontSize: 12
    },
    selectCity: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        width: '65%',
        marginBottom: 10
    },
    // Password input
    icon: { 
        width: 16,
        height: 16
    },
    showPassword: {
        right: 12,
        top: 95,
        position: 'absolute'
    },
    confirmIcon: { 
        width: 16,
        height: 16
    },
    showConfirmPassword: {
        right: 12,
        top: 163,
        position: 'absolute'
    },
    input: {
        backgroundColor: 'rgba(248, 248, 248, 1)',
        width: '100%',
        fontSize: 12,
        marginVertical: 5
    },
    registerInputs: {
        position: 'absolute',
        flex: 1,
        bottom: '0%',
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: 'rgba(248, 248, 248, 1)',
        borderTopLeftRadius: 20,
        borderTopRightRadius: 20,
        width: '100%',
        fontSize: 10,
        paddingVertical: 50
    },
    findGymsBtn: {
        width: '65%',
        alignItems: 'center',
        flex: 1,
        backgroundColor: 'rgba(39, 39, 39, 1)',
        borderRadius: 20,
        padding: 20,
        elevation: 5,
        shadowColor: 'rgba(39, 39, 39, 1)',
        marginTop: 5
    },
    findGymsNotice : {
        fontSize: 11,
        fontFamily: 'Inter-SemiBold',
        marginBottom: 10
    },
    findGymsLabel : {
        color: 'rgba(248, 248, 248, 1)',
        fontFamily: 'Inter-SemiBold',
    },
    login: {
        flexDirection: 'row',
        paddingTop: 15
    },
    loginLink: {
        color: 'blue',
        fontFamily: 'Inter-Bold',
    },
    // Gender
    selectGender: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        marginBottom: 10
    },
    selectGenderRadio: {
        flexDirection: 'row',
        alignItems: 'center',
        width: '30%',
    },
    selectGenderRadioTxt: {
        fontSize: 10,
        fontWeight: 'bold',
    }
});