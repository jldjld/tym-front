import React from 'react';
import { ImageBackground, StyleSheet, Text, TouchableOpacity, View } from 'react-native';

const HomeScreen = ({navigation}) => {
    return(
        <View style={styles.homeWrapper}>
            <ImageBackground source={require('../../assets/colifting_home_img.jpg')} style={styles.image}>
                <Text style={styles.homeTitle}>Tym .{"\n"}</Text>
                <Text style={styles.homeBaseline}>Trouver un partenaire {"\n"}d'entrainement en salle de sport</Text>
                <View style={styles.homeAuth}>
                    <TouchableOpacity
                    onPress={() => navigation.navigate('Login')}>
                        <Text style={styles.btnLabel}>Se connecter</Text>
                    </TouchableOpacity>
                    <TouchableOpacity style= {styles.buttonRight}
                    onPress={() => navigation.navigate('Register')}>
                        <Text style={styles.btnLabelRegister}>S'inscrire</Text>
                    </TouchableOpacity>
                </View>
            </ImageBackground>
        </View>
    )
}

const styles = StyleSheet.create({
    homeWrapper: {
        margin: 0,
        padding: 0,
    },
    image: {
        backgroundRepeat: 'no-repeat',
        backgroundAttachment: 'fixed',
        height: '100%',
        backgroundsize: 'cover',
        backgroundPosition: '75% 50%',
        position: 'relative',
    },
    homeTitle: {
        fontFamily: 'Inter-Bold',
        color: 'white',
        textTransform: 'uppercase',
        fontSize: 50,
        position: 'absolute',
        left: '10%',
        bottom: '28%',
    },
    homeBaseline: {
        color: 'white',
        fontSize: 15,
        fontFamily: 'Inter-SemiBold',
        position: 'absolute',
        bottom: '20%',
        left: '10%',
    },
    homeAuth: {
        position: 'absolute',
        display: 'flex',
        flexDirection: 'row',
        bottom: '0%',
        alignItems: 'center',
        justifyContent: 'space-between',
        backgroundColor: 'rgba(39, 39, 39, 1)',
        height: '15%',
        width: '100%',
        fontSize: 10,
        paddingLeft: 40,
        paddingRight: 40,
    },
    buttonRight: {
        backgroundColor: 'rgba(248, 248, 248, 1)',
        borderTopLeftRadius: 20,
        height: '100%',
        width: '70%',
        display: 'flex',
        marginLeft: 50,
        alignItems: 'center',
        justifyContent: 'center'
    },
    btnLabel: {
        color: 'white',
        fontFamily: 'Inter-Bold',
    },
    btnLabelRegister: {
        color: 'rgba(39, 39, 39, 1)',
        fontFamily: 'Inter-Bold',
    },
});

export default HomeScreen;