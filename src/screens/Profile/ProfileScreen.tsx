import { View, Text, StyleSheet } from 'react-native'
import React, { useRef, useState } from 'react'
import { useAuth } from '../../../contexts/AuthContext';

const ProfileScreen = ({navigation}) => {
  const {onLogout}          = useAuth();
	const [errMsg, setErrMsg] = useState('');
    


  const logout = async () => {
      try {
          const auth = await onLogout!();
          if (auth && auth.error) {
              setErrMsg(auth.msg);
              alert(auth.msg);
          }
      } catch (error) {
          setErrMsg(error);
      }
  };
  
  return (
    <View>
      <Text style={styles.errorNotice}>{ errMsg ? errMsg : "" }</Text>
      <Text>Profile</Text>
    </View>
  )
}

const styles = StyleSheet.create({
  errorNotice: {
    color: 'red',
    fontFamily: 'Inter-Bold',
    fontSize: 8,
    marginBottom: 15
  },
});

export default ProfileScreen;