export enum Availabilities {
    EARLY_MORNING = "6h - 9h",
    MORNING = "9h - 12h",
    LUNCH = "12h - 14h",
    AFTERNOON = "14h - 17h",
    LATE_AFTERNOON = "17h - 20h",
    EVENING = "20h - 22h"
}