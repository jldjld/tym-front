import { NavigationContainer } from '@react-navigation/native';
import React from 'react';
import AppStack from './AppStack';
import AuthStack from './AuthStack';
import { useAuth } from '../../contexts/AuthContext';

export default function AppNav() {
    const {authState, onLogout} = useAuth();


    return (
        <NavigationContainer >
            { authState?.authenticated ? <AuthStack /> : <AppStack />}
        </NavigationContainer> 
  )
}