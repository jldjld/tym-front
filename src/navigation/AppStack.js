import { createNativeStackNavigator } from '@react-navigation/native-stack';
import React from 'react'
import LoginScreen from '../screens/Auth/LoginScreen'
import RegisterScreen from '../screens/Auth/RegisterScreen'
import HomeScreen from '../screens/HomeScreen'

const Stack = createNativeStackNavigator();

export default function AppStack() {
  return (
    <Stack.Navigator 
        screenOptions={{
            headerShown: false
        }}>
        <Stack.Screen
            name="Tym"
            component={HomeScreen}
        />
        <Stack.Screen
            name="Login"
            component={LoginScreen}
            />
        <Stack.Screen
            name="Register"
            component={RegisterScreen}
        />
    </Stack.Navigator>
  )
}