import React from 'react'
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import { useAuth } from '../../contexts/AuthContext';
import { Button } from 'react-native';
import ProfileScreen from '../screens/Profile/ProfileScreen';

const Stack = createNativeStackNavigator();

export default function AuthStack() {
  const {authState, onLogout} = useAuth();


  return (
    <Stack.Navigator 
    screenOptions={{
        headerShown: true
    }}>
      <Stack.Screen
      name="Profile"
      component={ProfileScreen}
      options={{
        headerRight: () => <Button onPress={onLogout} title="Déconnexion" />
      }}
      />
    </Stack.Navigator>
  )
}