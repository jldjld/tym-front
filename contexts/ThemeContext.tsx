import React, { createContext } from 'react';
import image from '../assets/colifting_home_img.jpg';

export const imageBackground = {
    image: {
        backgroundRepeat: 'no-repeat',
        imageBackground: image,
        backgroundAttachment: 'fixed',
        height: '100%',
        backgroundsize: 'cover',
        backgroundPosition: '75% 50%',
        position: 'relative',
    }
}

export const ThemeContext = createContext(imageBackground);