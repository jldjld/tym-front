import { createContext, useContext, useEffect, useState } from "react";
import { AUTH_TOKEN, BASE_URL } from "../config";
import * as SecureStore from 'expo-secure-store';
import apiClient from "../src/services/customAxios";

interface AuthProps {
    authState?: { token: string | null; authenticated: boolean | null };
    onRegister?:  (email: string, password: string) => Promise<any>;
    onLogin?: (email: string, password: string) => Promise<any>;
    onLogout?: () => Promise<any>;
}

const AuthContext = createContext<AuthProps>({});

export const useAuth = () => {
    return useContext(AuthContext);
}

export const AuthProvider = ({children}: any) => {
    const [authState, setAuthState] = useState<{
        token: string | null;
        authenticated: boolean | null;
    }>({
        token: null,
        authenticated: null
    });

    useEffect(() => {
        const getToken = async () => {
            const token = await SecureStore.getItemAsync(AUTH_TOKEN);
            if( token ) {
                // Set Bearer token
                apiClient.defaults.headers.common['Authorization'] = 'Bearer' + token;

                setAuthState({
                    token: token,
                    authenticated: true
                });
            }
        }
        getToken();
    }, []);

    // Login method
    const login = async (email: string, password: string) => {
        try {
            const auth = await apiClient.post( BASE_URL + '/login', {email, password});
            setAuthState({
                token: auth.data.token,
                authenticated: true
            });

            // Set Bearer token
            apiClient.defaults.headers.common['Authorization'] = 'Bearer' + auth.data.token;

            // Store the token
            await SecureStore.setItemAsync( AUTH_TOKEN, auth.data.token);

            return auth;
        } catch (e) {
            return {error: true, msg: (e as any).response.data.msg };
        }
    };

    const logout = async () => {
        // Delete stored token
        await SecureStore.deleteItemAsync( AUTH_TOKEN );

        // Reset headers and authstate
        apiClient.defaults.headers.common['Authorization'] = '';

        setAuthState({
            token: null,
            authenticated: null
        });
        
    };

    const value = {
        onLogin: login,
        onLogout: logout,
        authState
    };

    return <AuthContext.Provider value={value}>{children}</AuthContext.Provider>;
}